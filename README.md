# TFM Doom

This repo contains all the files related with the Doom TFM made by David Lima in the CEI (Centro de Electrónica Industrial) at UPM (Universidad Politécnica de Madrid). 

The TFM main objective is to accelerate the graphic tasks execution of the DOOM Game in a zcu102 platform using all the resources available. Only source files are given but they are enough to generate the output files correctly folowing the instructions atatched to each project. 

The directory layout is as folows:

- Doom_Game: Vanilla DOOM and chocolate-doom source files.
- stretch4x_bare_metal__app: Sources files to create the vivado project 

## Getting started

The next tools were used:

- Vivado Design Tools 2018.1


## Authors 
* **David Lima** - *davidlimaastor@gmail.com* - [Bitbucket](https://bitbucket.org/d_lima)
## License 

## Acknowledgments

