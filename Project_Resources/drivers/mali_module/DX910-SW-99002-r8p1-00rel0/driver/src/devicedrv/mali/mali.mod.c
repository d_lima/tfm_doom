#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("of:N*T*Carm,mali-300");
MODULE_ALIAS("of:N*T*Carm,mali-300C*");
MODULE_ALIAS("of:N*T*Carm,mali-400");
MODULE_ALIAS("of:N*T*Carm,mali-400C*");
MODULE_ALIAS("of:N*T*Carm,mali-450");
MODULE_ALIAS("of:N*T*Carm,mali-450C*");
MODULE_ALIAS("of:N*T*Carm,mali-470");
MODULE_ALIAS("of:N*T*Carm,mali-470C*");

MODULE_INFO(srcversion, "533BB7E5866E52F63B9ACCB");
