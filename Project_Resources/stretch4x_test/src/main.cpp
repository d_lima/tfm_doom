#include "mainheader.h"

using namespace std;
using namespace cv;     // OpenCV

// Should be I_VideoBuffer
byte *src_buffer, *dest_buffer;
int dest_pitch;

int main(int argc, char** argv )
{

    // Read the input image
    Mat img = imread("doom.PPM",IMREAD_COLOR);
    imshow("Input image 320x200", img);

    dest_pitch = PITCH;

    // Output image, processed by the function
    Mat imgOut;

    // Print some info about the input image
    printf("Input image info: \n");
    printf("Number of channels: %d\n",img.channels());
    printf("Rows: %d\n",img.rows);
    printf("Cols: %d\n",img.cols);
    printf("Pixel size: %lu\n",img.elemSize());

    // Destination has 4 times the size of the source
    // function multiplies the rows by 4
    dest_buffer = (byte*)malloc(1280 * 960 * img.channels() * sizeof(byte) );

    // Convert source in bytes to be process
    src_buffer = matToBytes(img);

    byte* src_buffer_copy = src_buffer;

    printf("-------------------------\n");
    printf("Print first values of input image ... \n");
    for (int i=0; i<10; i++) {
        printf("Data: %u\n", *src_buffer_copy);
        src_buffer_copy++;
      }

    I_Stretch4x(X1, Y1, X2, Y2);

    imgOut = bytesToMat(dest_buffer, 1280  , 960);
    imwrite("doom_result.PPM", imgOut);

    imshow("Doom1", imgOut);

    free(src_buffer);
    free(dest_buffer);

	waitKey(0);
}
