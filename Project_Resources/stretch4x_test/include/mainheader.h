#ifndef MAINHEADER_H
#define MAINHEADER_H

#include <opencv2/opencv.hpp>

#include "doomtype.h"
#include <stddef.h>
#include <stdio.h>

#include "I_Stretch4x_Function.h"
#include "convert.h"

typedef unsigned char byte;

#define X1 (0)
#define X2 (320)
#define Y1 (0)
#define Y2 (200)

// Pitch of destination buffer, ie. screen->pitch.
#define PITCH (1280)


#endif // MAINHEADER_H
