#ifndef STRETCH4_H
#define STRETCH4_H


#include "doomtype.h"
#include <stddef.h>
#include <stdio.h>


// Screen resolution
#define SCREENWIDTH  (320)
#define SCREENHEIGHT (200)

// Lookup tables used for aspect ratio correction stretching code.
// stretch_tables[0] : 20% / 80%
// stretch_tables[1] : 40% / 60%
// All other combinations can be reached from these two tables.
static byte *stretch_tables[2] = { NULL, NULL };

boolean I_Stretch4x(int x1, int y1, int x2, int y2);


#endif // STRETCH4X_H
