#!/bin/sh

# Delete build directory
rm -r build/

# Make build directory
mkdir build

# Copy doom.ppm file into build
cp input_640_400.PPM build/

# Change to it and run cmake
cd build
cmake .. -DCMAKE_CXX_FLAGS="-std=c++11 -g"

# Compile with all cores
make -j4
