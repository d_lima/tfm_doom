#ifndef STRETCH4_H
#define STRETCH4_H


#include "doomtype.h"
#include <stddef.h>
#include <stdio.h>


// Screen resolution
#define SCREENWIDTH  (640)
#define SCREENHEIGHT (400)


boolean I_Stretch2x(int x1, int y1, int x2, int y2);


#endif // STRETCH4X_H
