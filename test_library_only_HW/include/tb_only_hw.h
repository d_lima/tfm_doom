/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	Stretch4x Accelerator camek project
 * 		File: - "tb_only_hw.h"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *	
 * 	
 * 	Simple cmake project to run the stretch4x_hw function on the zcu102. To run 
 * this correctly, you need to load the bitstream before.
 *
 * TODO:
 *
 * CHANGELOG:
 *
 *---------------------------------------------------------------------------*/
#ifndef TB_ONLY_HW_H
#define TB_ONLY_HW_H


/*----------------------------- Libraries -----------------------------------*/
#include <stdio.h>
#include <stdlib.h>

/*---------------------------- Project headers ------------------------------*/
#include "doomtype.h"
#include "hardware_f.h"

/*------------------------- Constant Definitions ----------------------------*/
#define SCREENWIDTH  (320)
#define SCREENHEIGHT (200)

/*------------------------- Variable Definitions ----------------------------*/



#endif // TB_ONLY_HW_H
