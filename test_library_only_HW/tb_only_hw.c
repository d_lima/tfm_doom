/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	Stretch4x SDSoC project
 * 		File: - "stretch4x_tb.cpp"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO:
 *
 * CHANGELOG:
 *     [20-11-2018]: File cha ged to use with SDSoC
 *
 *---------------------------------------------------------------------------*/
#include "tb_only_hw.h"


/*------------------------------- Main --------------------------------------*/
int main(int argc, char** argv )
{
	printf("Starting measure performance project ... \n");

    int number_values;

    FILE *f_input;

    byte *bytes_img_in;
    byte *bytes_img_out;

    printf("Open input.txt file ...");
    f_input = fopen("input.txt", "r");

    if (f_input==NULL) {fputs ("File error\n",stderr); exit (1);}

    printf("OK \n");

    printf("Read data from file ...");
    bytes_img_in = (byte *)malloc(64000 * sizeof(byte));
    if (bytes_img_in == NULL) {fputs ("Memory error img_in",stderr); exit (2);}

    bytes_img_out = (byte *)malloc(1228800 * sizeof(byte));
    if (bytes_img_out == NULL) {fputs ("Memory error img_out",stderr); exit (2);}

    number_values = fread (bytes_img_in, 1,64000, f_input);
    if (number_values != 64000) {fputs ("Reading error",stderr); exit (3);}
    fclose(f_input);
    printf("OK \n");


    /*----------------------------------
    ------------- Hardware -------------
    ----------------------------------*/
    printf("----------------------\n");
    printf("Executing Hardware implementation ...\n");

    printf("Calling the UUT ...");
    _p0_I_Stretch4x_HW_1_noasync(bytes_img_in, bytes_img_out);
    printf("OK\n");

    /* Free memory */
    free(bytes_img_in);
    free(bytes_img_out);
	
	    printf("Test OK !\n");
    return 0;


}
