/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	Stretch4x bare metal application project
 * 		File: - "main.h"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO:
 *
 * CHANGELOG:
 *     [24-10-2018]: Hello world
 *
 *---------------------------------------------------------------------------*/
#ifndef MAIN_H_
#define MAIN_H_

/*---------------------------- Include Files --------------------------------*/
#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"

#include "xaxidma.h"
#include "xparameters.h"
#include "xdebug.h"
#include "xi_stretch4x_hw.h"


/*------------------------- Constant Definitions ----------------------------*/
#define DMA_DEV_ID				XPAR_AXIDMA_0_DEVICE_ID
#define STRETCH4X_HW_ID			XPAR_I_STRETCH4X_HW_0_DEVICE_ID


/*
 * Device hardware build related constants.
 */

#define MEM_BASE_ADDR		0x00000000

#define TX_BUFFER_BASE		(MEM_BASE_ADDR + 0x00100000)
#define RX_BUFFER_BASE		(MEM_BASE_ADDR + 0x20000000)
#define RX_BUFFER_HIGH		(MEM_BASE_ADDR + 0x25FFFFFF)


/*------------------------- Variable Definitions ----------------------------*/
/*
 * Device instance definitions
 */
XAxiDma AxiDma;
XI_stretch4x_hw module_hw;

#define NUMBER_PIXELS_INPUT  (320*200)  // 64000
#define NUMBER_PIXELS_OUTPUT (1280*960) // 1228800

// Pointer to the file loaded from psu_init_dow_input.tcl
unsigned char* input_data = (unsigned char*)0x65000000;

/*------------------------- Function Prototypes -----------------------------*/
int devicesReset(u16 DeviceIdDMA, u16 DeviceIdModule);
int DMATransfer(u16 DeviceId);
int DMAReceive(u16 DeviceId);


#endif /* MAIN_H_ */
